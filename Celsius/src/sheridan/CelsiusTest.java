package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromFahrenheit() {
		assertTrue("Conversion is incorrect", Celsius.convertFromFahrenheit(32) == 0);
	}
	
	@Test
	public void testConvertFromFahrenheitException() {
		assertFalse("Conversion is incorrect", Celsius.convertFromFahrenheit(32) != 0);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryIn() {
		assertTrue("Conversion is incorrect", Celsius.convertFromFahrenheit(36) == 2);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryOut() {
		assertTrue("Conversion is incorrect", Celsius.convertFromFahrenheit(35) == 2);
	}
	
	

}
