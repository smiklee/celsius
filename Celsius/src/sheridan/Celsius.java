package sheridan;

public class Celsius {

	public static int convertFromFahrenheit(int temp) {
		
		float part1 = (float)temp - 32;
		float part2 = part1 * 5;
		float part3 = part2 / 9;
		
		
		return Math.round(part3);
	}
	
	public static void main(String[] args) {
		int temp = 0;
		System.out.println("Result:" +convertFromFahrenheit(temp));
	}
}
